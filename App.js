import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './reducers';
import { Home, Chat } from './components';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));
const { Navigator, Screen } = createStackNavigator();

export default class App extends React.PureComponent {

  render() {
    return (
    <Provider store={store}>
      <NavigationContainer>
        <Navigator
          initialRouteName="Home"
          screenOptions={{
            headerStyle: {
              backgroundColor: '#555',
            },
            headerTintColor: '#fff',
          }}
        >
          <Screen name="Home" component={Home} options={{ title: 'ChatApp' }}/>
          <Screen name="Chat" component={Chat} options={({ route: { params }}) => ({ title: `Salon ${params.room}` })} />
        </Navigator>
      </NavigationContainer>
    </Provider>
    );
  }
}